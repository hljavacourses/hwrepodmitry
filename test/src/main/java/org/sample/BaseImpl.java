package org.sample;

import java.util.concurrent.TimeUnit;
import jdk.Counter;
import jdk.Impl;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;

public class BaseImpl {

  @Benchmark @OutputTimeUnit(TimeUnit.NANOSECONDS)
  public void testMethod(MyState state) {
    state.counter.inc();
  }


  @State(Scope.Thread)
  public static class MyState {

    public Counter counter;

    @Setup
    public void setup() {
      counter = new Impl();
    }

    @TearDown
    public void teardown() {
      System.out.println(counter.get());
    }
  }
}

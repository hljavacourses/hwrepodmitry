package jdk;

import jdk.Counter;
import jdk.Impl;

public class EmptyDecorator implements Counter {

  private Counter origin;

  public EmptyDecorator(Counter origin) {
    this.origin = origin;
  }

  @Override
  public void inc() {
    origin.inc();
  }

  @Override
  public int get() {
    return origin.get();
  }
}

package jdk;

public final class FinalImpl implements Counter {

  int counter;

  @Override
  public void inc() {
    counter++;
  }

  @Override
  public int get() {
    return counter;
  }
}

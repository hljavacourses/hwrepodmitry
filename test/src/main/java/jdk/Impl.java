package jdk;

public class Impl implements Counter {

  int counter;

  @Override
  public void inc() {
    counter++;
  }

  @Override
  public int get() {
    return counter;
  }
}

package jdk;

public interface Counter {

  void inc();

  int get();
}

import java.io.IOException;
import my.dbrutski.servletcontainer.HttpServer;
import test.TestServlet;

public class MainTest {

  public static void main(String[] args) throws IOException {
//    HttpServer.withServlet("/", new TestServlet()).start();
    new HttpServer().start();
  }
}

package test;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class TestServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    resp.getWriter()
        .append("<html>"
            + "<head>"
            + "<title>"
            + "Example"
            + "</title>"
            + "</head>"
            + "<body>"
            + "<p>"
            + "This is an example of a simple HTML page with one paragraph."
            + "</p>"
            + "</body>"
            + "</html>");
  }
}

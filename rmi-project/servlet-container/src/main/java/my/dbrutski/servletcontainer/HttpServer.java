package my.dbrutski.servletcontainer;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import javax.servlet.Servlet;
import test.TestServlet;

public class HttpServer {

  private List<Entry<String, Servlet>> servlets = new ArrayList<>();

  public HttpServer withServlet(String s, TestServlet testServlet) {
    servlets.add(new SimpleEntry<>(s, testServlet));
    return this;
  }

  public void start() throws IOException {
    final com.sun.net.httpserver.HttpServer server = com.sun.net.httpserver.HttpServer
        .create(new InetSocketAddress("localhost", 9090), 10);
    server.createContext("/", new HttpHandler() {
      @Override
      public void handle(HttpExchange httpExchange) throws IOException {
        final String response = "<html>"
            + "<head>"
            + "<title>"
            + "Example"
            + "</title>"
            + "</head>"
            + "<body>"
            + "<p>"
            + "This is an example of a simple HTML page with one paragraph."
            + "</p>"
            + "</body>"
            + "</html>";
        httpExchange.sendResponseHeaders(200, response.length());
        new PrintStream(httpExchange.getResponseBody()).append(response)
            .close();
      }
    });
    server.start();
  }
}

package com.example.demo;

import domain.service.UserServiceImpl;
import domain.support.CommandManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.Random;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import ui.controller.UserController;

@SpringBootApplication
public class DemoApplication {

  public static void main(String[] args) {
    SpringApplication.run(DemoApplication.class, args);
  }

  @Bean
  UserController userController() throws RemoteException {
    return new UserController(
        new UserServiceImpl(new CommandManager(LocateRegistry.getRegistry("localhost", 2005)),
            new Random()));
  }
}

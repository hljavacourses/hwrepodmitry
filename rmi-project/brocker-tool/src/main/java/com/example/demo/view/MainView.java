package com.example.demo.view;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Setter;
import com.vaadin.flow.function.ValueProvider;
import com.vaadin.flow.router.Route;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import ui.controller.UserController;

@Route
public class MainView extends VerticalLayout {

  User user = new User();
  Binder<User> binder = new Binder<>(User.class);
  @Autowired
  UserController controller;

  public MainView() {
    FormLayout form = new FormLayout();
    add(form);
    TextField name = new TextField();
    form.addFormItem(name, "Customer Name");
    IntegerField id = new IntegerField();
    form.addFormItem(id, "Customer Identity");
    TextField password = new TextField();
    form.addFormItem(password, "Customer Password");

    binder.bind(name, User::getName, User::setName);
    binder.bind(id, composeNullable(User::getId, Long::intValue),
        composeNullable(User::setId, Integer::longValue));
    binder.bind(password, User::getPasswd, User::setPasswd);
    add(new HorizontalLayout(
        getCustomerButton(),
        updateUser(),
        addUser(),
        deleteCustomer()
    ));
  }

  private Button updateUser() {
    Button button = new Button("Update Customer");
    button.addClickListener(this::updateUserListener);
    return button;
  }

  private Button deleteCustomer() {
    Button button = new Button("Delete Customer");
    button.addClickListener(x -> {
      if (binder.writeBeanIfValid(user)) {
        controller.removeUser(this.user.getId());
        binder.readBean(new User());
      } else {
        throw new RuntimeException("invalid user");
      }
    });
    return button;
  }

  private Button addUser() {
    Button button = new Button("Add Customer");
    button.addClickListener(this::updateUserListener);
    return button;
  }

  private void updateUserListener(
      ClickEvent<Button> x) {
    if (binder.writeBeanIfValid(user)) {
      User user = controller.updateUser(this.user);
      binder.readBean(user);
    } else {
      throw new RuntimeException("invalid user");
    }
  }

  private Button getCustomerButton() {
    Button button = new Button("Get Customer");
    button.addClickListener(x -> {
      if (binder.writeBeanIfValid(user)) {
        controller.getUsers().stream().filter(y -> y.getId().equals(user.getId())).findFirst()
            .ifPresent(binder::readBean);
      } else {
        throw new RuntimeException("invalid user");
      }
    });
    return button;
  }

  private <T, E, R> ValueProvider<T, R> composeNullable(ValueProvider<T, E> first,
      ValueProvider<E, R> second) {
    return x -> {
      E apply = first.apply(x);
      return apply == null ? null : second.apply(apply);
    };
  }

  private <T, E, R> Setter<T, E> composeNullable(Setter<T, R> setter, ValueProvider<E, R> mapper) {
    return (o, v) -> {
      if (v != null) {
        setter.accept(o, mapper.apply(v));
      }
    };
  }
}

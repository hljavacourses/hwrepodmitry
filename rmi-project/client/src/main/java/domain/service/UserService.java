package domain.service;

import java.util.List;
import model.User;
import model.UserTO;

public interface UserService {

    List<UserTO> getUsers();

    void removeUser(final Long id);

    UserTO updateUser(final UserTO user);
}

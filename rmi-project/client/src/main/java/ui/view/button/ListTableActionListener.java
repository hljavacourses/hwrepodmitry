package ui.view.button;

import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JTable;
import model.User;

public abstract class ListTableActionListener implements ActionListener {

    protected JTable table;
    protected List<User> list;

    public void setTable(JTable table) {
        this.table = table;
    }

    public void setList(List<User> list) {
        this.list = list;
    }
}

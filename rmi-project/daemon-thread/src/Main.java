public class Main {

  public static void main(String[] args) {
    Thread thread = new Thread(() -> {
      try {
        while (true) {
          System.out.println("alive");
          Thread.sleep(1000);
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    });
    thread.setDaemon(true);
    thread.start();
  }
}

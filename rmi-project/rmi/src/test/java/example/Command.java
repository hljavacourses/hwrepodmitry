package example;

/**
 * Общий интерфейс команды
 */
public interface Command {
  String execute(String arg);
}

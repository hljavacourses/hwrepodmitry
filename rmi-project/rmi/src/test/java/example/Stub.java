package example;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.net.Socket;
import java.util.Arrays;

/**
 * Прокси комманды. Работает в процессе клиента
 */
public class Stub implements Command, Serializable {

  private final String host;
  private final int port;

  public Stub(String host, int port) {
    this.host = host;
    this.port = port;
  }

  public Stub() {
    host = "localhost";
    port = 8080;
  }

  public String execute(String arg) {
    try {
      try (final Socket socket = new Socket(host, port)) {
        final ObjectOutputStream oos = new ObjectOutputStream(
            socket.getOutputStream());
        final Method method = Command.class.getMethod("execute", String.class);
        final Class[] signature = Arrays.stream(method.getParameters()).map(Parameter::getType)
            .toArray(Class[]::new);
        oos.writeObject("execute");
        oos.writeObject(signature);
        oos.writeObject(arg);
        oos.flush();
        final ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
        return (String) ois.readObject();
      }
    } catch (IOException | NoSuchMethodException | ClassNotFoundException e) {
      throw new RuntimeException(e);
    }
  }
}

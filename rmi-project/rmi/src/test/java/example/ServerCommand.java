package example;

/**
 * Реальная команда. Работает в процессе сервера
 */
public class ServerCommand implements Command {

  public String execute(String arg) {
    return "Hello, " + arg;
  }
}

package example;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;

public class Client {

  public static void main(String[] args) throws IOException, ClassNotFoundException {
    // Обращается к реестку за сериализуемым объектом прокси
    try (Socket registry = new Socket("localhost", 8081)) {
      final ObjectInputStream ois = new ObjectInputStream(registry.getInputStream());
      final Command stub = (Command) ois.readObject();

      // Вызывает метод на прокси
      System.out.println(stub.execute("World"));
    }
  }
}

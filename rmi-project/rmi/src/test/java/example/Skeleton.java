package example;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Класс обрабатывающий обращения к комманде по сокету.
 */
public class Skeleton {

  private int port;
  private Command origin;

  public Skeleton(int port, Command origin) {
    this.port = port;
    this.origin = origin;
  }

  public void start() {
    try {
      try (ServerSocket serverSocket = new ServerSocket(port)) {
        System.out.println("Server proxy listens port " + port);
        while (true) {
          try (Socket connection = serverSocket.accept()) {
            System.out.println("Server proxy accepted connection");
            final ObjectInputStream ois = new ObjectInputStream(
                connection.getInputStream());
            final String name = (String) ois.readObject();
            final Class<?>[] signature = (Class<?>[]) ois.readObject();
            final String argument = (String) ois.readObject();
            final Method method = origin.getClass().getMethod(name, signature);
            System.out.println(String
                .format("Server proxy invoke method [%s], with arguments [%s]", name, argument));
            final Object result = method.invoke(origin, argument);
            final ObjectOutputStream oos = new ObjectOutputStream(
                connection.getOutputStream());
            oos.writeObject(result);
            oos.flush();
          } finally {
            System.out.println("Connection closed");
          }
        }
      } finally {
        System.out.println("Server proxy stopped");
      }
    } catch (IOException | ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }
}

package example;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

  public static void main(String[] args) {
    // Поток для обработки порта реестра
    new Thread(() -> {
      try {
        final int registryPort = 8081;
        try (ServerSocket registry = new ServerSocket(registryPort)) {
          System.out.println("Registry listens on port " + registryPort);
          while (true) {
            try (Socket connection = registry.accept()) {
              final ObjectOutputStream oos = new ObjectOutputStream(
                  connection.getOutputStream());
              // При образении к реестру создается объект прокси и отсылается клиенту
              oos.writeObject(new Stub("localhost", 8080));
            }
          }
        } finally {
          System.out.println("Registry stopped");
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }).start();
    // Поток обработки реальной команды
    new Thread(() -> {
      try {
        final Skeleton serverCommand = new Skeleton(8080, new ServerCommand());
        serverCommand.start();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }).start();
  }
}

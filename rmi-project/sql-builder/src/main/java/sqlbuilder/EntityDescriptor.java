package sqlbuilder;

import static java.util.stream.Collectors.toList;

import exceptions.ProcessException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;
import model.Entity;

public class EntityDescriptor {

  private Class<? extends Entity> clazz;

  public EntityDescriptor(Class<? extends Entity> clazz) {
    this.clazz = clazz;
  }

  String tableName() {
    return clazz.getSimpleName();
  }

  public String idName() {
    return "id";
  }

  public List<String> columns() {
    return Stream
        .concat(Stream.of("id"), Arrays.stream(clazz.getDeclaredFields()).map(Field::getName))
        .collect(toList());
  }

  List<String> columnsWithoutId() {
    return Arrays.stream(clazz.getDeclaredFields()).map(Field::getName)
        .filter(f -> !idName().equals(f)).collect(toList());
  }

  <T extends Entity> List<Object> values(T entity) {
    try {
      List<Object> list = new ArrayList<>();
      for (Field f : clazz.getDeclaredFields()) {
        f.setAccessible(true);
        Object o = f.get(entity);
        list.add(o);
      }
      return list;
    } catch (IllegalAccessException e) {
      throw new ProcessException(e);
    }
  }

  public <T extends Entity> List<Object> valuesWithoutId(T entity) {
    try {
      List<Object> list = new ArrayList<>();
      for (Field f : clazz.getDeclaredFields()) {
        if (!f.getName().equals(idName())) {
          f.setAccessible(true);
          Object o = f.get(entity);
          list.add(o);
        }
      }
      return list;
    } catch (IllegalAccessException e) {
      throw new ProcessException(e);
    }
  }

  public <T extends Entity> T newInstance() {
    try {
      for (Constructor<?> ctr : clazz.getConstructors()) {
        if (ctr.getParameterCount() == 0) {
          return (T) ctr.newInstance();
        }
      }
      throw new IllegalStateException(
          "No default constructor for class " + clazz.getCanonicalName());
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new RuntimeException(e);
    }
  }

  public <T extends Entity> T setValues(T entity, ResultSet rs) {
    try {
      for (String column : columns()) {
        Object value = rs.getObject(column);
        Field field = getField(column, clazz);
        field.setAccessible(true);
        field.set(entity, value);
      }
      return entity;
    } catch (SQLException | IllegalAccessException e) {
      throw new RuntimeException(e);
    }
  }

  private Field getField(String column, Class<?> clazz) {
    try {
      Objects.requireNonNull(clazz);
      return clazz.getDeclaredField(column);
    } catch (NoSuchFieldException e) {
      return getField(column, clazz.getSuperclass());
    }
  }
}

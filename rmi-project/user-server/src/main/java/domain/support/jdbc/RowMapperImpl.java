package domain.support.jdbc;

import java.sql.ResultSet;
import model.Entity;
import sqlbuilder.EntityDescriptor;

public class RowMapperImpl<T extends Entity> implements RowMapper<T> {

  private final EntityDescriptor descr;

  public RowMapperImpl(Class<T> obj) {
    descr = new EntityDescriptor(obj);
  }

  @Override
  public T rowMap(final ResultSet rs) {
    T result = descr.newInstance();
    return descr.setValues(result, rs);
  }
}

package server;

import command.ExceptionHandlerServerCommandManager;
import command.ServerCommandManager;
import command.ServerCommandManagerImpl;
import dao.jdbc.UserJdbcDao;
import domain.support.jdbc.ConnectionPool;
import domain.support.jdbc.JdbcDaoSupport;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.rmi.AlreadyBoundException;
import java.rmi.Remote;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import model.User;
import model.UserTO;
import org.postgresql.ds.PGSimpleDataSource;

public class App {

  public static final ExecutorService executorService = Executors.newFixedThreadPool(10);

  public static Long currentUserId = 0L;
  public static List<UserTO> users;

  static {
    users = new ArrayList<>();
    User user = new User();
    user.setId(++currentUserId);
    user.setName("Pavel");
    user.setPasswd("MyNewPasswordIs******");
    users.add(user);


  }

  public static void main(String[] args)
      throws IOException, AlreadyBoundException, InterruptedException {

    Registry registry = LocateRegistry.createRegistry(2005);

    UserJdbcDao userDao;
    try (InputStream is = App.class.getResourceAsStream("/db.properties")) {
      Properties props = new Properties();
      props.load(is);
      String jdbcClassName = props.getProperty("jdbc.driver.class");
      String jdbcUrl = props.getProperty("db.url");
      String login = props.getProperty("db.username");
      String passwd = props.getProperty("db.password");
      int min = Integer.parseInt(props.getProperty("db.min"));
      int max = Integer.parseInt(props.getProperty("db.max"));
      PGSimpleDataSource ds = new PGSimpleDataSource();
      ds.setURL(jdbcUrl);
      ds.setUser(login);
      ds.setPassword(passwd);
      userDao = new UserJdbcDao(new JdbcDaoSupport(new ConnectionPool(min, max, ds)));
    }

    ExceptionHandlerServerCommandManager scm = new ExceptionHandlerServerCommandManager(
        new ServerCommandManagerImpl(registry, executorService));

    try {
      ((ServerCommandManagerImpl) scm.getOrigin()).initializeCommands(userDao);
    } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
      e.printStackTrace();
    }

    Remote remoteServerCommandManager = UnicastRemoteObject.exportObject(scm, 2006);
    registry.rebind(ServerCommandManager.class.getName(), remoteServerCommandManager);

    System.out.println("Server has been started successfully on port 2006");
  }
}

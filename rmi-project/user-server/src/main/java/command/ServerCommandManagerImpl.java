package command;

import annotation.ServerCommand;
import dao.UserDao;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import model.TransferObject;
import org.reflections.Reflections;

public class ServerCommandManagerImpl implements ServerCommandManager {

  //  private Map<Class, Command> commands;
  private Registry registry;
  private ExecutorService executorService;

  public ServerCommandManagerImpl(Registry registry,
      ExecutorService executorService) {
    this.registry = registry;
    this.executorService = executorService;
  }

  @Override
  public <T, D extends TransferObject> D execute(final Class<T> clazz, D obj)
      throws Exception {

    Command command = (Command) registry.lookup(clazz.getSimpleName());
    Future<D> future = executorService.submit(new Worker<D>(command, obj));
    return future.get();
  }

  public void initializeCommands(UserDao user)
      throws RemoteException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

    Reflections reflections = new Reflections("command");
    Set<Class<?>> commandClasses = reflections.getTypesAnnotatedWith(ServerCommand.class);

    for (Class<?> commandClass : commandClasses) {
      Constructor<?> constructor = commandClass.getConstructor(UserDao.class);
      Command command = (Command) constructor.newInstance(user);

//      commands.put(command.getClass(), command);

      Remote commandInstance = UnicastRemoteObject.exportObject(command, 2005);
      final ServerCommand annotation = commandClass.getAnnotation(ServerCommand.class);
      String commandName = annotation.name();
      registry.rebind(commandName, commandInstance);
    }

  }
}

package command;

import annotation.ServerCommand;
import dao.UserDao;
import java.rmi.Remote;
import java.rmi.RemoteException;
import model.User;
import model.UserTO;

@ServerCommand(name = "CreateUserCommand")
public class CreateUserCommandImpl implements CreateUserCommand,
    Remote {

  private UserDao userDao;

  public CreateUserCommandImpl(UserDao userDao) throws RemoteException {
    this.userDao = userDao;
  }

  @Override
  public UserTO execute(final UserTO user) throws RemoteException {
    if (user.getId() == null) {
      User newUser = new User();
      newUser.setName(user.getName());
      newUser.setPasswd(user.getPasswd());
      userDao.create(newUser);
      user.setId(newUser.getId());
      return user;
    } else {
      User newUser = new User();
      newUser.setId(user.getId());
      newUser.setName(user.getName());
      newUser.setPasswd(user.getPasswd());
      userDao.update(newUser);
      return user;
    }
  }
}

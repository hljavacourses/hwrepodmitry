package command;

import annotation.ServerCommand;
import dao.UserDao;
import java.util.List;
import model.GetUsersTO;
import model.User;

@ServerCommand(name = "GetUsersCommand")
public class GetUsersCommandImpl implements GetUsersCommand {

  private UserDao dao;

  public GetUsersCommandImpl(UserDao dao) {
    this.dao = dao;
  }

  @Override
  public GetUsersTO execute(GetUsersTO obj) {
    List<User> users = dao.findAll();
    obj.getUsers().clear();
    users.forEach(obj.getUsers()::add);
    return obj;
  }
}

package container;

import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import org.junit.Before;

public class MyContainerInfraTest {

  @Before
  public void setup() throws NamingException {
    Properties env = new Properties();
    env.put("java.naming.factory.initial", "");
    Context ctx = new InitialContext(env);
    ctx.rebind("cn=test", "value2");
  }
}

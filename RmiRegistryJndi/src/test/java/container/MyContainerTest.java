package container;

import static org.junit.Assert.assertEquals;

import javax.annotation.Resource;
import org.junit.Test;

public class MyContainerTest {

  @Test
  public void shouldReturnRegisteredBean() {
    MyContainer cnt = new MyContainer();
    cnt.add(BeanA.class);
    assertEquals(BeanA.class, cnt.get(BeanA.class).getClass());
  }

  @Test
  public void shouldReturnSubtype() {
    MyContainer cnt = new MyContainer();
    cnt.add(BeanA.class);
    assertEquals(BeanA.class, cnt.get(Bean.class).getClass());
  }

  @Test
  public void shouldInjectResourceFromJndi() {
    MyContainer cnt = new MyContainer(key -> "test");
    cnt.add(ValuedBean.class);
    assertEquals("test", cnt.get(ValuedBean.class).getValue());
  }

  public interface Bean {

  }

  public static class BeanA implements Bean {

  }

  public static class ValuedBean {

    @Resource(name = "value")
    private String value;

    public String getValue() {
      return value;
    }
  }
}
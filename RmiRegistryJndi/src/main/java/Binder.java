import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Binder {

  public static void main(String[] args)
      throws NamingException {
    Context ctx = new InitialContext();
    ctx.rebind("cn=test", "value2");
  }
}

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameClassPair;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;

public class Browser {

  public static void main(String[] args) {
    try {
      Context context = new InitialContext();
      System.out.println(context.lookup(""/*"cn=test"*/));
      NamingEnumeration<NameClassPair> names = context.list("");
      while (names.hasMoreElements()) {
        System.out.println(names.nextElement());
      }
    } catch (NamingException e) {
      e.printStackTrace();
    }
  }

  public static void traverse(Context ctx, int level) throws NamingException {
    System.out.println(emptyString(level) + ctx.getNameInNamespace());
    NamingEnumeration<NameClassPair> names = ctx.list("");
    while (names.hasMoreElements()) {
      final NameClassPair element = names.nextElement();
      if (element instanceof DirContext) {
        traverse((DirContext) element, level + 1);
      } else {
//        traverse(element, level + 1);
      }
    }
  }

  public static void traverse(DirContext ctx, int level) {

  }

  public static String emptyString(int size) {
    java.lang.String result = "";
    for (int i = 0; i < size; i++) {
      result += " ";
    }
    return result;
  }
}

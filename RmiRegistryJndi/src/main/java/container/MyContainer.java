package container;

import static java.lang.String.format;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import javax.annotation.Resource;

public class MyContainer {

  private List<Class<?>> beans = new ArrayList<>();
  private Function<Object, Object> lookup;

  public MyContainer(Function<Object, Object> lookup) {
    this.lookup = lookup;
  }

  public MyContainer() {
    this(x -> null);
  }

  void add(Class<?> bean) {
    beans.add(bean);
  }

  public <T> T get(Class<T> beanType) {
    try {
      for (Class<?> bean : beans) {
        if (beanType.isAssignableFrom(bean)) {
          return createInstance(bean);
        }
      }
      throw new IllegalArgumentException(format("Bean for type %s is not found", beanType));
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private <T> T createInstance(Class<?> bean)
      throws InstantiationException, IllegalAccessException, InvocationTargetException {
    Constructor<?>[] constructors = bean.getConstructors();
    for (Constructor<?> constructor : constructors) {
      if (0 == constructor.getParameterCount()) {
        T result = (T) constructor.newInstance();
        Field[] fields = bean.getDeclaredFields();
        for (Field field : fields) {
          for (Annotation annotation : field.getDeclaredAnnotations()) {
            if (
                Resource.class.isAssignableFrom(annotation.getClass())
            ) {
              String name = ((Resource) annotation).name();
              field.setAccessible(true);
              field.set(result, lookup.apply(name));
            }
          }
        }
        return result;
      }
    }
    throw new IllegalArgumentException(format("Bean %s have no default constructor", bean));
  }
}
